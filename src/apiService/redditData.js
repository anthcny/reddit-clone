export const apiConfig = {
    proxyadress: "https://cors-anywhere.herokuapp.com/",
    url: 'https://www.reddit.com/',
    categories: 'subreddits/popular.json?limit=10',
    posts: 'search.json?limit=20&q='
}

function getApiUrl(query){
    return `${apiConfig.proxyadress + apiConfig.url + query}`
}

export function getRedditData(query){
    return fetch(getApiUrl(query))
        .then(response => {
            return response.json();
        })
        .then(data => {
            return data;
        })
        .catch(console.log)
}