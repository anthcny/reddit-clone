import React, { Component } from 'react';
import '../../styles/style.css';
import {getRedditData} from '../../apiService/redditData';
import CommentList from '../Home/commentList';


export default class Category extends Component {
    state = {
        loading: false,
        post: {},
        comments: []
    }

    componentDidMount = () => {
        const {category, id} = this.props.match.params,
            query = `/r/${category}.json?id=${id}&limit=1`;

        this.getDataFromApi(query)
            .then(data => {
                data = data.data.children[0].data;
                this.setState({post: data, loading: false});
                return data;
            })
            .then((post)=>{
                this.getComments(post);
            })
    }

    getDataFromApi = (query) => {
        this.setState({loading: true})
        return getRedditData(query);
    };

    getComments = (post) => {
        const query = post.permalink + '.json';
        if(!query) return;

        getRedditData(query)
                    .then(data => {
                        //console.log(data[1]);
                        this.setState({comments: data[1].data.children});
                    })
    }
    
    render() {
        const { post, loading, comments } = this.state;
        if(loading) return (
            <div>
                <span>loading...</span>
            </div>
        )

        return (
            <div >
                <p>Title: {post.title}</p>
                <p>{post.selftext}</p>
                <br/>
                {comments.length !== 0 && <CommentList comments={comments}/>}
            </div>
        );      
    }
}