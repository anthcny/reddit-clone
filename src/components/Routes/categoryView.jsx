import React, { Component } from 'react';
import '../../styles/style.css';
import {getRedditData} from '../../apiService/redditData';
import Post from '../Home/post';
import {connect} from 'react-redux';
import { saveCategory } from '../../AC';


class CategoryView extends Component {
    state = {
        limit: 10,
        posts: [],
        loading: false,
        after: null,
        before: null
    }

    getPosts = (isAfter) => {
        this.setState({loading: true})
        getRedditData(this.getApiQuery(isAfter))
            .then(data => {
                const posts = data.data.children,
                    after = data.data.after,
                    before = data.data.before;

                this.setState({loading: false, posts, after, before});
            })
    }

    getApiQuery = (isAfter) => {
        const { name } = this.props,
            { limit, after, before } = this.state;
        let query;
        if (isAfter) {
            query = `/r/${name}.json?limit=${limit}&after=${after}`;
        }
        else query = `/r/${name}.json?limit=${limit}&before=${before}`;

        return query;
    }

    render() {
        const {name} = this.props;
        const {posts} = this.state;
        const postList = posts.map(post => {
            return (
                <Post category={`${name}`} key={post.data.id} post={post}/>
                )
        });

        return (
            <div className="category">
                { name }
                <div>
                    {postList}
                </div>
            </div>
        );      
    }
}

function mapStateToProps(state){
    return {
        category: state.categoriesViews
    }
}

export default connect(mapStateToProps, { saveCategory })(CategoryView);