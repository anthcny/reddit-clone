import React, { Component } from 'react';
import Comment from './comment';
import '../../styles/style.css';

export default class CommentList extends Component {
    static defaultProps = {
        comments: []
    }

    state = {
        commentsLimit: 2,
        repliesLimit: 1,
        openNodes: new Set()
    }
    
    showMoreComments = () => {
        const commentsLimit = this.state.commentsLimit + 7
        this.setState({
            commentsLimit
        });
    }

    getCommentsView = (comments, limit) => {
        var commentsPart;
        if ( !limit ) {
            commentsPart = comments;
        }
        else commentsPart = comments.slice(0, limit);
        return (
            <div>
                {
                    commentsPart.map((comment)=>{
                        var node = comment.data.id;
                        if(comment.data.replies){
                            let replies,
                                hideRepliesCount = 0;
                            if(this.state.openNodes.has(node)){
                                replies = null;
                            }
                            else {
                                replies = this.state.repliesLimit;
                                hideRepliesCount = comment.data.replies.data.children.length - 1 - replies;
                            }
                            return (
                                <div key={node+'_reply'} className="comments">
                                    <Comment key={node} comment={comment}/>
                                    <div className="replyComment">
                                        {this.getCommentsView(comment.data.replies.data.children, replies)}
                                    </div>
                                   { hideRepliesCount > 0 && <button onClick={()=> { 
                                                let openNodes = new Set(this.state.openNodes.values())
                                                    openNodes.add(node);
                                                this.setState({openNodes})
                                            }
                                        }>
                                        {hideRepliesCount} more replies
                                    </button>
                                    }
                                </div>
                            )
                        }
                        return (
                                <Comment key={node} comment={comment}/> 
                        )
                    })
                }
            </div>
        )
    }

    render() {
        const {comments} = this.props
        const limited = comments.length > this.state.commentsLimit;
        console.log('update', this.state.openNodes.values())
        return (
            <div>
                <p>Comments:</p>
                {this.getCommentsView(comments, this.state.commentsLimit)}
                {limited && <button onClick={this.showMoreComments}>Show more comments</button>}
            </div>
        );
    }
}