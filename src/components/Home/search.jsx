import React, { Component } from 'react';
import '../../styles/style.css';
import SearchButton from './searchButton';

export default class Search extends Component {
    state = {
        searchText: this.props.text || ''
    }

    // static getDerivedStateFromProps(nextProps, prevState){
    //     if(this.props.text) return {
    //         searchText: this.props.text
    //     }
    // }
    
    search = () => {
        const query = this.state.searchText;
        if(query === '') return;
        this.props.search(query);
    }

    setTextValue = (event) => {
        this.setState({searchText: event.target.value});
    }

    cancel = () => {
        this.setState({searchText: ''})
        this.props.cancelSearch();
        //this.searchLine.value = '';
    }

    render() {
        const { searchText } = this.state;
        return (
            <div className="flexRow">
                <input value={searchText} className="searchLine" type="text" placeholder="enter text" onChange={this.setTextValue}/>
                {/* <input className="searchButton" type="submit" value='Search' onClick={this.search}/> */}
                <SearchButton query={searchText} click={this.search}/>
                <input className="searchButton" type="submit" value='Cancel' onClick={this.cancel}/>
            </div>
        );      
    }

    // setInputRef = (domEl) => {
    //     this.searchLine = domEl;
    // }
}