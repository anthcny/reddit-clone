import React, { Component } from 'react';
import '../../styles/style.css';
import Post from './post';

export default class PostsList extends Component {
    static defaultProps = {
        posts: [],
        loading: false
    }

    render() {
        const {loading, posts} = this.props;
        
        if(loading) return (
            <div>
                loading...
            </div>
        )

        const postsList = posts.map((post)=>{
            return (
                <Post key={post.data.id} post={post} category={post.data.subreddit}/>
                )
        });

        return (
            <div>
                {postsList}
            </div>
        );
    }
}