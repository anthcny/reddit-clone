import React, { Component } from 'react';
import '../../styles/style.css';
import Post from './post';
import {Link} from 'react-router-dom';

export default class Category extends Component {
    static defaultProps = {
        posts: []
    }

    render() {
        const {name, posts, categoryName} = this.props;
        const postList = posts.map(post => {
            return (
                <Post category={`${categoryName}`} key={post.data.id} post={post}/>
                )
        });


        return (
            <div>
                <Link className="category" to={`/r/${categoryName}`}>{ name }</Link>
                <div>
                    {postList}
                </div>
            </div>
        );      
    }
}