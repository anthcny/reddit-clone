import React, { Component } from 'react';
import '../../styles/style.css';
import {getRedditData, apiConfig} from '../../apiService/redditData';
import CategoriesList from './categoriesList';
import Search from './search';
import PostsList from './postsList';
import {Route} from 'react-router-dom';

export default class Home extends Component {
    state = {
        searching: false,
        loading: false,
        posts: []
    }

    static getDerivedStateFromProps(nextProps, prevState){
        if(nextProps.query) return {
            query: nextProps.query
        }
        else return null;
    }

    componentDidMount = () => {
        const {query} = this.state;
        if (query){
            this.searchPosts(query);
        }
    }

    searchPosts = (query) => {
        if(query === '') return;
        this.setState({searching: true, loading: true});
        getRedditData(apiConfig.posts + query)
        .then(data => {
            data = data.data.children;
            this.setState({posts: data, loading: false});
        })
    }
    
    cancelSearching = () => {
        this.setState({searching: false, loading: false})
    }

    render() {
        const {searching, loading, posts} = this.state;
        return (
            <div>
                <Search search={this.searchPosts} cancelSearch={this.cancelSearching}/>
                { !searching && <CategoriesList /> }
                { searching && <Route path='/search/q=:q' component={()=><PostsList loading={loading} posts={posts}/>}/>}
                {/* { searching && <PostsList loading={loading} posts={posts}/> } */}
            </div>
        );      
    }

    getPosts = () => {

    } 
}