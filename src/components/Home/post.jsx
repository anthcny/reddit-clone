import React from 'react';
import '../../styles/style.css';
import {Link} from 'react-router-dom';

export default function Post(props) {
    const { title, id} = props.post.data,
            {category} = props;
    return (
        <div>
            <Link className="post" to={`/r/${category}/id=${id}`}>{title}</Link>
        </div>
    );
    
}  

// class WrappedLink extends Component{
//     componentDidMount = () => {
//         ReactDOM.findDOMNode(this).addEventListener('click', (event) => {
//           event.stopPropagation();
//         }, false);
//     }

//     render(){
//         return (
//             this.props.link
//         )
//     }
// }

// const noBubbling = (component) => {
//     const click = (e) => {
//         console.log('handled click', e);
//         e.preventDefault()
//         e.stopPropagation();
//         e.nativeEvent.stopImmediatePropagation();
//     };
//     //(e)=> e.stopPropagation()
//     return <span onClick={click}>{component}</span>

//     //return <WrappedLink link={component}/>
// }