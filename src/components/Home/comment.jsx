import React from 'react';
import '../../styles/style.css';

export default function Comment(props){
    if(!props.comment) return null;

    const {author, body} = props.comment.data;
    
    return (
        <div className="comment">
            {author && <p>Author: <strong>{author}</strong></p>}
            {body && <span>{body}</span>}
        </div>
    );
}