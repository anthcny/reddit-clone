import React from 'react';
import '../../styles/style.css';
import {Link} from 'react-router-dom';

export default function SearchButton(props) {
    const { query, click } = props;
    return (
        <button className="searchButton" onClick={click}>
            {!query && 'Search'}
            {query && <Link to={`/search/q=${query}`}>Search</Link>}
        </button>
    );
}