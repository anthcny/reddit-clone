import React, { Component } from 'react';
import Category from './category';
import {getRedditData, apiConfig} from '../../apiService/redditData';
import {connect} from 'react-redux'
import {saveCategories} from '../../AC';

class CategoriesList extends Component {
    static defaultProps = {
        list: []
    }

    state = {
        posts: {},
        loading: false,
        categories: []
    }

    static isEmpty = (obj) => {
        if(!obj) return false;
        for (const key in obj) {
          return false;
        }
        return true;
      }

    static getDerivedStateFromProps(nextProps, state){
        //console.log('props', nextProps, 'epmty', CategoriesList.isEmpty(nextProps.categories))
        if(!CategoriesList.isEmpty(nextProps.categories)){
            return {
                categories: nextProps.categories.categories.categories,
                posts: nextProps.categories.categories.posts
            }
        }
        else return null;
    }

    componentDidMount = () => {
        if (this.state.categories.length > 0) return;
        this.getDataFromApi(apiConfig.categories)
        .then(data => {
          data = data.data.children;
          this.setState({categories: data, loading: false});
          //categoriesState = data;
          //console.log(data);
          return data;
        }).then((data) => {
            this.getPosts(data)
        })
    }
    
    getDataFromApi = (query) => {
        this.setState({loading: true})
        return getRedditData(query);
    };

    getPosts = (categories) => {
        let posts = {};
        Promise.all(categories.map(category => {
            return getRedditData(category.data.display_name_prefixed + '.json?limit=2')
            .then(data => {
                posts[category.data.display_name] = data.data.children;
                return Promise.resolve();
            }) 
        })) 
        .then(() => {
            this.setState({posts: posts});
            this.props.saveCategories({categories: this.state.categories, posts: posts});
            //postsState = posts;
        })
        .catch(console.log);
    };


    render() {
        const { categories, loading, posts } = this.state;
        if(loading) return (
            <div>
                <span>loading...</span>
            </div>
        )

        const list = categories.map((category)=>(
            <Category key={category.data.display_name} posts={posts[category.data.display_name]} name={category.data.public_description} categoryName={category.data.display_name}/>
        ));

        return (
            <div>
                {list}
            </div>
        );
    }
}

function mapStateToProps(state){
    return {
        categories: state.categories
    }
}

export default connect(mapStateToProps, { saveCategories })(CategoriesList)

