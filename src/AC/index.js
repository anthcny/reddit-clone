import {CATEGORIES} from '../constants';
import {CATEGORY_VIEW} from '../constants';

export function saveCategories(categories) {
    const action = {
        type: CATEGORIES,
        payload: { categories }
    }

    return action;
}

export function saveCategory(category) {
    const action = {
        type: CATEGORY_VIEW,
        payload: { category }
    }

    return action;
}