import React, { Component } from 'react';
import Home from '../../components/Home/home';
import {BrowserRouter, Route} from 'react-router-dom';
import creacteBrowserHistory from 'history/createBrowserHistory';
import PostView from '../../components/Routes/postView';
import CategoryView from '../../components/Routes/categoryView';
import store from '../../store';
import {Provider} from 'react-redux';


var history = creacteBrowserHistory();

class App extends Component {
  getPosts = ({match}) => {
      return <Home query={match.params.q} />
  }

  getCategory = ({match}) => {
    return <CategoryView name={match.params.name} />
  }
  
  render() {
    return (
      <Provider store={store}>
        <BrowserRouter history={history}>
          <div>
              <Route exact path="/" component={Home}/>
              <Route path="/r/:category/id=:id" component={PostView}/>
              <Route path="/search/q=:q" render={this.getPosts}/>
              <Route exact path="/r/:name" render={this.getCategory}/>
          </div>
        </BrowserRouter>
      </Provider>
    );
  }
}

export default App;
