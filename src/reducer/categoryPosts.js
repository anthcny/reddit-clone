import {CATEGORY_VIEW} from '../constants';

export default (state = {}, action) => {
    const {type, payload} = action;

    switch(type){
        case CATEGORY_VIEW:
            {
                const newstate = {...state, categoriesViews: {[payload.category.name]: payload.category}};
                return newstate;
            }
        default: break;
    }

    return state;
}