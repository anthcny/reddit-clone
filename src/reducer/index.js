import {combineReducers} from 'redux'
import categories from './categories';
import categoryPosts from './categoryPosts';

export default combineReducers({
    categories,
    categoryPosts
})