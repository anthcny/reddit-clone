import {CATEGORIES} from '../constants';

export default (state = {}, action) => {
const {type, payload} = action;

switch(type){
    case CATEGORIES:
        {
            const newstate = {...state, categories: payload.categories};
            return newstate;
            // state.categories = payload.categories;
            // return state;
        }
    default: break;
}

    return state;
}